import SparqlHttp from "sparql-http-client";
import {
    searchForNamedGraphsQuery,
    getAllNamespaces,
    getTopTwenty,
    searchMultipleTypes,
    searchInSection,
    loadAllMetadataQuery, getClassesCount, getPropertiesCount, getObjectsCount, getVocabulariesForGraph
} from "./sparql";
import fetch from 'isomorphic-fetch';
import {SparqlJsonParser} from "sparqljson-parse";
import { matchPrefixAndReturnColour } from '../api/utils';
import ApiClient from "./client";

const sparqlJsonParser = new SparqlJsonParser();
SparqlHttp.fetch = fetch;

export async function load(endpointUrl) {
    const endpoint = new SparqlHttp({ endpointUrl });

    try {
        const parsedResult = await request(endpoint, searchForNamedGraphsQuery);
        return Promise.all(parsedResult.map(async (item) => {
            if (item.graph.value === 'http://merck.github.io/Halyard/ns#statsContext') {
                return {
                    dataset: {
                        metadata: await getDatasetMetadata(endpoint),
                        namespaces: await getDatasetNamespaces(endpoint),
                        subjects: await getDatasetTopTwentySubjects(endpoint),
                        objects: await getDatasetTopTwentyObjects(endpoint),
                        properties: await getDatasetTopTwentyProperties(endpoint),
                        counts: await getObjectCounts(endpoint),
                    },
                    error: false,
                };
            } else {
                return {
                    error: true,
                    e: new Error("No statsContext inside dataset")
                }
            }
        }));
    } catch (e) {
        return {
            error: true,
            e: new Error("Invalid SPARQL endpoint")
        };
    }

}

export async function matchNamespaceIRI(namespaces) {

    try {
        const updatedNamespaces = Promise.all(namespaces.map(async (namespace) => {
            const match = await matchPrefixAndReturnColour(namespace.objectNamespace.value);
            if (match) {
                return {
                    ...match,
                    triples: namespace.tripleSum.value,
                    entities: namespace.entities.value,
                };
            }
            return null;
        }));

        let prefixedNamespaces = await updatedNamespaces;
        return prefixedNamespaces.filter(namespace => namespace);
    } catch (e) {
        return {
            error: true,
            e: new Error("Invalid SPARQL endpoint")
        };
    }
}


async function getDatasetMetadata(endpoint) {
    try {
        const response = await endpoint.selectQuery(loadAllMetadataQuery);
        const body = await response.text();
        const result = JSON.parse(body);
        return sparqlJsonParser.parseJsonResults(result);
    } catch (e) {
        return {
            error: true,
            e
        };
    }
}

async function getDatasetNamespaces(endpoint) {
    try {
        const response = await endpoint.selectQuery(getAllNamespaces);
        const body = await response.text();
        const result = JSON.parse(body);
        return sparqlJsonParser.parseJsonResults(result);
    } catch (e) {
        return {
            error: true,
            e
        };
    }
}



async function getDatasetTopTwenty(endpoint, type) {
    try {
        const response = await endpoint.selectQuery(getTopTwenty(type));
        const body = await response.text();
        const result = JSON.parse(body);
        return sparqlJsonParser.parseJsonResults(result);
    } catch (e) {
        return {
            error: true,
            e
        };
    }
}

async function getDatasetTopTwentySubjects(endpoint) {
    return getDatasetTopTwenty(endpoint, 'subject');
}

async function getDatasetTopTwentyObjects(endpoint) {
    return getDatasetTopTwenty(endpoint, 'object');
}

async function getDatasetTopTwentyProperties(endpoint) {
    return getDatasetTopTwenty(endpoint, 'property');
}


export function createSparqlHttp(endpointUrl) {
    return new SparqlHttp({ endpointUrl });
}

export async function search(endpointUrl, types, search, limit = null, offset = null, graph = null) {

    const endpoint = new SparqlHttp({ endpointUrl });

    try {
        const response = await endpoint.selectQuery(searchMultipleTypes(types, search, limit, offset, graph));
        const body = await response.text();
        const result = JSON.parse(body);
        return sparqlJsonParser.parseJsonResults(result);
    } catch (e) {
        return {
            error: true,
            e
        }
    }
}

export async function searchInSections(endpointUrl, section, search, offset = null, limit = null) {

    const endpoint = new ApiClient(endpointUrl);

    try {
        const response = await endpoint.selectQuery(searchInSection(section, search, offset, limit));
        const body = await response.text();
        const result = JSON.parse(body);
        return sparqlJsonParser.parseJsonResults(result);
    } catch (e) {
        return {
            error: true,
            e
        }
    }
}

export async function getGraphVocabs(endpointUrl, graph) {
    const endpoint = new SparqlHttp({ endpointUrl });

    try {
        const response = await endpoint.selectQuery(getVocabulariesForGraph(graph));
        const body = await response.text();
        const result = JSON.parse(body);
        return sparqlJsonParser.parseJsonResults(result);
    } catch (e) {
        return {
            error: true,
            e
        }
    }

    // return await performAjaxRequest(endpoint, getVocabulariesForGraph, graph);
}

async function getObjectCounts(endpoint) {

    let classesCnt = await request(endpoint, getClassesCount);
    classesCnt = classesCnt[0].entities.value;

    let propertiesCnt = await request(endpoint, getPropertiesCount);
    propertiesCnt = propertiesCnt[0].entities.value;

    let objectsCnt = await request(endpoint, getObjectsCount);
    objectsCnt = objectsCnt[0].entities.value;

    return {
        classesCnt,
        propertiesCnt,
        objectsCnt
    };
}

async function request(endpoint, sparql, ...params) {

    try {
        let response = {};
        if (typeof sparql === "function") {
            response = await endpoint.selectQuery(sparql(params));
        } else {
            response = await endpoint.selectQuery(sparql);
        }
        const body = await response.text();
        const result = JSON.parse(body);
        return sparqlJsonParser.parseJsonResults(result);
    } catch (e) {
        return {
            error: true,
            e
        }
    }
}
