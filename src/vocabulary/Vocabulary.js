import React, {Component} from 'react';
import './vocabulary.scss';
import {ErrorAlert, LoadingSpinner} from "../common";
import {search} from "../api/api";
import ReactPaginate from 'react-paginate';
import {getPrefixInSpanAndText} from "../api/utils";

class Vocabulary extends Component {

    state = {
        vocabularies: [],
        selectedVocabularyPrefix: '',
        selectedVocabularyIRI: '',
        vocabularyItems: [],
        vocabularyItemsCount: [],
        endpoint: null,
        datasetLoadFailed: null,
        loading: false,
        limit: 20,
        offset: 0,
    };

    componentDidMount = async () => {
        let dataset = localStorage.getItem(`dataset:${this.props.match.params.name}`);
        dataset = JSON.parse(dataset);
        let vocabularies = dataset.metadata.namespaces;
        vocabularies.sort((a, b) => b.triples - a.triples);

        this.setState({
            vocabularies,
            endpoint: dataset.endpoint
        });
    };

    handleSelectVocabulary = async (e) => {
        this.setState({
            loading: true,
        });
        let dataset = localStorage.getItem(`dataset:${this.props.match.params.name}`);
        dataset = JSON.parse(dataset);
        let vocabularies = dataset.metadata.namespaces;
        const item = vocabularies[vocabularies.findIndex(item => item.prefix === e.target.value)];

        let data = await search(this.state.endpoint, ['subject', 'property', 'object'], item.iri);

        if (!data.error) {

            this.setState({
                selectedVocabularyPrefix: item.prefix,
                selectedVocabularyIRI: item.iri,
                vocabularyItems: data.slice(0, this.state.limit),
                vocabularyItemsCount: data.length,
                loading: false,
            });
        } else {
            this.setState({
                loading: false,
                datasetLoadFailed: data.e,
            });
        }
    };

    handleCloseAlert = () => {
        this.setState({
            datasetLoadFailed: null,
        });
    };

    changeLimit = (e) => {
        this.setState({
            limit: e.target.value
        });
    };

    generateTableItem = (data) => {
        return data.map((item, index) => {
            let changedText = getPrefixInSpanAndText(item.object.value);

            let text = changedText.text;
            let prefix = changedText.prefix;
            return (
                <tr key={index}>
                    <td><span className="hint--bottom" aria-label={item.object.value}>{prefix}{text}</span></td>
                    <td>{item.prop.value.replace('http://ldf.fi/void-ext#', '').replace('http://rdfs.org/ns/void#', '')}</td>
                    <td className="font-sans-serif">{item.triples.value}</td>
                </tr>
            );
        });
    };

    paginateChange = async (offset) => {
        window.scrollTo(0, 0);
        this.setState({
            loading: true
        });

        let data = await search(this.state.endpoint, ['subject', 'property', 'object'], this.state.selectedVocabularyIRI, this.state.limit, offset.selected);

        if (!data.error) {
            this.setState({
                vocabularyItems: data,
                loading: false,
                offset: offset.selected,
            });
        } else {
            this.setState({
                loading: false,
                datasetLoadFailed: data.e,
            });
        }
    };

    render() {
        return (
            <>
                {this.state.datasetLoadFailed ?
                    <ErrorAlert
                        message={this.state.datasetLoadFailed.message}
                        handleCloseAlert={this.handleCloseAlert}
                    />
                    : ""
                }
                {this.state.loading ? <LoadingSpinner/> : ""}
                <div className="content-sub-header">
                    <div className="left-side">
                        <h3 className="title">Vocabularies</h3>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card">
                                <div className="card-head">
                                    <div className="card-head-label">
                                        <h3 className="title">Dataset vocabularies</h3>
                                        <small>select vocabulary and explore its properties</small>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="col-xl-12">
                                        {this.state.vocabularies.map(vocab => {
                                            return <button
                                                type="button"
                                                className="btn btn-info"
                                                key={vocab.color}
                                                onClick={this.handleSelectVocabulary}
                                                value={vocab.prefix}
                                                style={{
                                                    backgroundColor: '#fff',
                                                    color: '#000',
                                                    marginRight: "0.5rem",
                                                    border: `4px solid ${vocab.color}`,
                                                    borderRadius: '10px',
                                                    fontWeight: "bolder"
                                                }}
                                            >
                                                {vocab.prefix}</button>
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.selectedVocabularyPrefix ?
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-xl-12">
                                <div className="card">
                                    <div className="card-head">
                                        <div className="card-head-label">
                                            <h3 className="title">{this.state.selectedVocabularyPrefix.charAt(0).toUpperCase() + this.state.selectedVocabularyPrefix.slice(1)}</h3>
                                        </div>
                                    </div>
                                    <div className="card-body card-content-center">
                                        <table className="table">
                                            <colgroup>
                                                <col width="60%"/>
                                                <col width="20%"/>
                                                <col width="20%"/>
                                            </colgroup>
                                            <thead>
                                            <tr>
                                                <th>NAME</th>
                                                <th>TYPE</th>
                                                <th>TOTAL</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {this.state.vocabularyItems.length > 0 ?
                                                this.generateTableItem(this.state.vocabularyItems)
                                                : <tr>
                                                    <td colSpan={4}>Nothing to display</td>
                                                </tr>}
                                            </tbody>
                                        </table>
                                    </div>
                                    {this.state.vocabularyItemsCount && this.state.vocabularyItems.length > 0 ?
                                        <div className="card-footer">
                                            <div className="pagination-box">
                                                <div className="col-xl-8">
                                                    <ReactPaginate
                                                        onPageChange={this.paginateChange}
                                                        pageCount={Math.ceil(parseFloat(this.state.vocabularyItemsCount / this.state.limit))}
                                                        pageRangeDisplayed={5}
                                                        marginPagesDisplayed={2}
                                                        containerClassName={'pagination'}
                                                        subContainerClassName={'pages pagination'}
                                                        activeClassName={'active'}
                                                        breakClassName={'page-link'}
                                                        pageClassName={'page-item'}
                                                        pageLinkClassName={'page-link'}
                                                        previousClassName={'page-item'}
                                                        nextClassName={'page-item'}
                                                        previousLinkClassName={'page-link'}
                                                        nextLinkClassName={'page-link'}/>
                                                </div>
                                                <div className="col-xl-4">
                                                    <div className="content-to-end">
                                                        <select
                                                            value={this.state.limit}
                                                            onChange={this.changeLimit}
                                                        >
                                                            <option value={10}>10</option>
                                                            <option value={20}>20</option>
                                                            <option value={50}>50</option>
                                                        </select>
                                                        <span className="pagination-count">
                                                        {`${1 + (this.state.limit * this.state.offset)}
                                                        - ${(this.state.limit * (this.state.offset + 1)) > this.state.vocabularyItemsCount ? this.state.vocabularyItemsCount : (this.state.limit * (this.state.offset + 1))}
                                                        of ${this.state.vocabularyItemsCount}`}
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        : ""
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    : ""
                }
            </>
        );
    }
}

export default Vocabulary;
