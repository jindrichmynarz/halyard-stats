export default class ApiClient {

    constructor(endpointUrl) {
        this.endpointUrl = endpointUrl;
        this.types = {
            select: {
                accept: 'application/sparql-results+json',
            }
        }
    }

    selectQuery (query, options) {
        options = options || {};

        options.accept = options.accept || this.types.select.accept;
        options.headers = {};
        const url = (options.endpointUrl || this.endpointUrl) + '?query=' + encodeURIComponent(query);

        options.method = 'get';
        options.headers['Accept'] = options.accept;

        return fetch(url, options)
    }

}
