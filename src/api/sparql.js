
export const loadAllMetadataQuery = `PREFIX sd: <http://www.w3.org/ns/sparql-service-description#>
PREFIX halyard: <http://merck.github.io/Halyard/ns#>
PREFIX void: <http://rdfs.org/ns/void#>
PREFIX void-ext: <http://ldf.fi/void-ext#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

SELECT ?graph ?prop ?obj
    WHERE {
      GRAPH halyard:statsContext {
    	{
         halyard:statsRoot sd:namedGraph ?graph . 
        ?graph a sd:NamedGraph;
             ?prop ?obj .
      	FILTER(?prop != sd:name) .
      	FILTER(?prop != sd:graph) .
      } UNION {
      	halyard:statsRoot ?prop ?obj .
      	BIND('default' AS ?graph) .
      }
        FILTER(?prop != void:propertyPartition) .
        FILTER(?prop != void-ext:objectPartition) .
        FILTER(?prop != void-ext:subjectPartition) .
        FILTER(?prop != rdf:type) .
        FILTER(?prop != sd:defaultGraph) .
      } 
    }
    ORDER BY (isIRI(?graph))
`;

export const searchForNamedGraphsQuery = `
    SELECT DISTINCT ?graph 
    WHERE {
      GRAPH ?graph { [] ?p [] }
    }
`;

export const getAllNamespaces = `PREFIX halyard: <http://merck.github.io/Halyard/ns#>
PREFIX void: <http://rdfs.org/ns/void#>
SELECT ?objectNamespace ?entities ?tripleSum{
  {
    SELECT ?objectNamespace (COUNT(DISTINCT ?o) AS ?entities) (SUM(?triples) AS ?tripleSum) {
      GRAPH halyard:statsContext {
		?s a void:Dataset;
        void:triples ?triples;
        ?p ?o .
        FILTER(isIRI(?o))
        FILTER(?s != halyard:statsRoot) 
        FILTER(?o != void:Dataset)
        BIND(str(?o) AS ?str)
        FILTER(CONTAINS(?str,"/") || CONTAINS(?str,"#"))
        BIND (IRI(REPLACE(?str,"(.*[#/]).*","$1")) AS ?objectNamespace)
      }
    }
    GROUP BY ?objectNamespace
    ORDER BY DESC(?entities)
  }
}
`;

export function getVocabulariesForGraph(graph){

    return `PREFIX halyard: <http://merck.github.io/Halyard/ns#>
PREFIX void: <http://rdfs.org/ns/void#>
PREFIX void-ext: <http://ldf.fi/void-ext#>
SELECT ?objectNamespace ?entities ?tripleSum{
  {
    SELECT ?objectNamespace (COUNT(DISTINCT ?o) AS ?entities) (SUM(?triples) AS ?tripleSum) {
      GRAPH halyard:statsContext {
		?s a void:Dataset;
        void:triples ?triples;
        ?p ?o .
        <${graph}> void-ext:objectPartition|void-ext:subjectPartition|void:propertyPartition ?s .
        FILTER(isIRI(?o))
        FILTER(?s != halyard:statsRoot) 
        FILTER(?o != void:Dataset)
        BIND(str(?o) AS ?str)
        FILTER(CONTAINS(?str,"/") || CONTAINS(?str,"#"))
        BIND (IRI(REPLACE(?str,"(.*[#/]).*","$1")) AS ?objectNamespace)
      }
    }
    GROUP BY ?objectNamespace
    ORDER BY DESC(?entities)
  }
}`;

}


export function getTopTwenty(type, ) {

    const prop = returnPropOfType(type).prop;

    return `PREFIX halyard: <http://merck.github.io/Halyard/ns#>
PREFIX void: <http://rdfs.org/ns/void#>
PREFIX void-ext: <http://ldf.fi/void-ext#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        SELECT ?object ?triples {
          GRAPH halyard:statsContext {
            ?s a void:Dataset;
               void:triples ?triples;
               ${prop} ?object .
          }
        }
        ORDER BY DESC(?triples)
        LIMIT 20
    `;
}

function returnPropOfType(type) {
    let prop = '';
    let graph = '';
    if (type === 'property') {
        prop = `void:property`;
        graph = `void:propertyPartition`;
    } else if (type === 'subject') {
        prop = `void-ext:subject`;
        graph = `void-ext:subjectPartition`;
    } else if (type === 'object') {
        prop = `void-ext:object`;
        graph = `void-ext:objectPartition`;
    }

    return {prop, graph};
}

// search with prefix => search = http://schema.org/{text}

// search in named graph: ?graph <http://ldf.fi/void-ext#objectPartition> ?s . ( + subject, property )

/*
types = [ subject, property, object ]
 */
export function searchMultipleTypes(types, search, limit = null, offset = null, graph = null) {

    let limitConstr = '';
    let offsetConstr = '';

    if (limit) {
        limitConstr = `LIMIT ${limit}`;

        if (offset) {
            offsetConstr = `OFFSET ${offset * limit}`;
        }
    }

    let prop = '?prop';
    let filterStatement = '';
    let graphStatement = '';
    let partitionFilters = '';
    let bindProperty = '';

    if (types.length === 1) {
        prop = returnPropOfType(types[0]).prop;
        bindProperty = `BIND(${prop} AS ?prop) .`;
    } else if (types.length === 2) {
        let filter = ['subject', 'property', 'object'].filter(item => !types.includes(item));
        console.log(filter)
        const classInFilter = returnPropOfType(filter[0]).prop;
        filterStatement = `FILTER(?prop != ${classInFilter}) .`;
    } else {
        // ?prop is not specified
        if (types.length === 0 || types.length === 3) {
            partitionFilters = `FILTER(?prop != void:propertyPartition) .
            FILTER(?prop != void-ext:objectPartition) .
            FILTER(?prop != void-ext:subjectPartition) .`;
        }
        if (graph) {
            graphStatement = `?graph void-ext:objectPartition|void-ext:subjectPartition|void:propertyPartition ?s .`;
        }
    }

    return `PREFIX halyard: <http://merck.github.io/Halyard/ns#>
PREFIX void: <http://rdfs.org/ns/void#>
PREFIX void-ext: <http://ldf.fi/void-ext#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    SELECT ?object ?triples ?prop {
      GRAPH halyard:statsContext {
        [] a void:Dataset;
           void:triples ?triples;
           ${prop} ?object .
        ${graphStatement}   
        BIND(str(?object) AS ?str) .
        ${bindProperty}
        ${filterStatement}
        FILTER(regex(?str,'${search}', 'i')) .
        ${partitionFilters}
      }
    }
    ORDER BY DESC(?triples)
    ${limitConstr}
    ${offsetConstr}
`;
}
// TODO: sums from subjects and objects
export function searchInClasses(search, offset = null, limit = null, graph = null) {
    let limitConstr = '';
    let offsetConstr = '';

    if (limit) {
        limitConstr = `LIMIT ${limit}`;

        if (offset) {
            offsetConstr = `OFFSET ${offset * limit}`;
        }
    }

    return `PREFIX halyard: <http://merck.github.io/Halyard/ns#>
PREFIX void: <http://rdfs.org/ns/void#>
PREFIX void-ext: <http://ldf.fi/void-ext#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    SELECT ?object ?triples ?prop {
      GRAPH halyard:statsContext {
        ?s a void:Dataset;
           void:triples ?triples;
           ?prop ?object .

        FILTER(?prop != void:property) .
        FILTER(?object != void:Dataset) .
        FILTER(?s != halyard:statsRoot) .
        BIND(str(?object) AS ?str) .
        FILTER(isIRI(?object)) .
        FILTER(regex(?str,'${search}', 'i')) .
      }
    }
    ORDER BY DESC(?triples)
    ${limitConstr}
    ${offsetConstr}
`;
}

function searchIn(type, search, offset = null, limit = null, graph = null) {
    const prop = returnPropOfType(type).prop;
    let limitConstr = '';
    let offsetConstr = '';

    if (limit) {
        limitConstr = `LIMIT ${limit}`;

        if (offset) {
            offsetConstr = `OFFSET ${offset * limit}`;
        }
    }

    return `PREFIX halyard: <http://merck.github.io/Halyard/ns#>
PREFIX void: <http://rdfs.org/ns/void#>
PREFIX void-ext: <http://ldf.fi/void-ext#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    SELECT ?object ?triples {
      GRAPH halyard:statsContext {
        [] a void:Dataset;
           void:triples ?triples;
           ${prop} ?object .

        BIND(str(?object) AS ?str) .
        FILTER(regex(?str,'${search}', 'i')) .
      }
    }
    ORDER BY DESC(?triples)
    ${limitConstr}
    ${offsetConstr}
`;
}

export function searchInSection(section, search, offset = null, limit = null, graph = null) {

    if (section === 'entities') {
        return searchInClasses(search, offset, limit, graph);
    } else if (section === 'properties') {
        return searchIn('property', search, offset, limit, graph);
    } else if (section === 'objects') {
        return searchIn('object', search, offset, limit, graph);
    }
}

export function getClassesCount() {
    return `PREFIX halyard: <http://merck.github.io/Halyard/ns#>
PREFIX void: <http://rdfs.org/ns/void#>
PREFIX void-ext: <http://ldf.fi/void-ext#>
        SELECT (COUNT(?object) AS ?entities) {
          GRAPH halyard:statsContext {
            ?s a void:Dataset;
           void:triples ?triples;
           void-ext:object|void-ext:subject ?object .

            FILTER(?object != void:Dataset) .
            FILTER(?s != halyard:statsRoot) .
            FILTER(isIRI(?object)) .
          }
        }
    `;
}

export function getPropertiesCount() {
    return getSectionCount('property');
}

export function getObjectsCount() {
    return getSectionCount('object');
}


function getSectionCount(type) {
    const prop = returnPropOfType(type).prop;

    return `PREFIX halyard: <http://merck.github.io/Halyard/ns#>
PREFIX void: <http://rdfs.org/ns/void#>
PREFIX void-ext: <http://ldf.fi/void-ext#>
        SELECT (COUNT(?object) AS ?entities) {
          GRAPH halyard:statsContext {
            ?s a void:Dataset;
           void:triples ?triples;
           ${prop} ?object .
           
            FILTER(?object != void:Dataset) .
            FILTER(?s != halyard:statsRoot) .
          }
        }
    `;
}
