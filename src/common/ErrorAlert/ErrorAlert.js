import React from 'react';
import PropTypes from 'prop-types';

function ErrorAlert({message, handleCloseAlert}) {
    return (
        <div className="alert alert-danger" role="alert">
            <div className="alert-icon"><i className="fas fa-exclamation" /></div>
            <div className="alert-text">{message}</div>
            <div className="alert-close">
                <i className="fa fa-times"
                   onClick={handleCloseAlert}/>
            </div>
        </div>
    );
}

ErrorAlert.propTypes = {
    message: PropTypes.string,
    handleCloseAlert: PropTypes.func
};

export default ErrorAlert;
