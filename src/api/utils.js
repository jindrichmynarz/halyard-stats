import prefixes from "../all.file";
import React from "react";
import {Redirect} from "react-router-dom";

export async function setPrefixes() {
    if (!localStorage.getItem('prefixes')) {
        try {
            let response = await fetch('https://prefix.cc/popular/all.file.json');
            const text = await response.text();
            localStorage.setItem('prefixes', text);
        } catch (e) {
            // fallback
            localStorage.setItem('prefixes', JSON.stringify(prefixes));
        }
    }
}

// https://stackoverflow.com/questions/3426404/create-a-hexadecimal-colour-based-on-a-string-with-javascript#answer-21682946
export function stringToColour(str) {
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    let colour = '#';
    for (let i = 0; i < 3; i++) {
        let value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
}

export function matchPrefix(prefixIRI) {
    let prefixes = localStorage.getItem('prefixes');
    prefixes = JSON.parse(prefixes);
    return Object.keys(prefixes).find(key => prefixes[key] === prefixIRI);
}

export function getPrefixInSpanAndText(iri) {
    let prefix = '';
    if (iri.indexOf('#') !== -1) {
        prefix = iri.substring(0, iri.indexOf('#') + 1);
        const prefixcc = matchPrefix(prefix);
        if (prefixcc) {
            prefix = <span className="font-weight-bold">{prefixcc}:</span>;
        }
        iri = iri.substring(iri.indexOf('#') + 1, iri.length);
    } else if (iri.indexOf('/') !== -1) {
        prefix = iri.substring(0, iri.lastIndexOf('/') + 1);
        const prefixcc = matchPrefix(prefix);
        if (prefixcc) {
            prefix = <span className="font-weight-bold">{prefixcc}:</span>;
        }
        iri = iri.substring(iri.lastIndexOf('/') + 1, iri.length);
    }

    return {text: iri, prefix};
}

export async function matchPrefixAndReturnColour(prefixIRI) {
    await setPrefixes();
    const match = matchPrefix(prefixIRI);
    if (match) {
        return {
            prefix: match,
            iri: prefixIRI,
            color: stringToColour(prefixIRI)
        };
    }
    return false;
}

export function getDatasetNamespaces(datasetName) {
    return getDataset(datasetName).metadata.namespaces;
}

export function getDatasetMetadata(datasetName) {
    return getDataset(datasetName).metadata.metadata;
}

export function getDataset(datasetName) {
    return JSON.parse(localStorage.getItem(`dataset:${datasetName}`));
}

export function generateVocabulariesChart(vocabularies) {
    vocabularies.sort((a, b) => b.triples - a.triples);
    let labels = [];
    let colors = [];
    let values = [];

    vocabularies.forEach(item => {
        labels.push(item.prefix);
        colors.push(item.color);
        values.push(item.triples);
    });

    return {
        labels,
        datasets: [
            {
                label: 'Dataset vocabularies',
                backgroundColor: colors,
                data: values,
            }
        ]
    }
}

export function generateMetadataChart(metadata, def = false) {
    metadata.sort((a, b) => b.obj.value - a.obj.value);
    let labels = [];
    let values = [];

    metadata.forEach(item => {
        if (parseInt(item.obj.value)) {
            if (def) {
                if (item.graph.value === 'default') {
                    labels.push(
                        item.prop.value
                            .replace('http://rdfs.org/ns/void#', '')
                            .replace('http://ldf.fi/void-ext#', '')
                            .replace('http://www.w3.org/ns/sparql-service-description#', '')
                    );
                    values.push(item.obj.value);
                }
            } else {
                labels.push(
                    item.prop.value
                        .replace('http://rdfs.org/ns/void#', '')
                        .replace('http://ldf.fi/void-ext#', '')
                        .replace('http://www.w3.org/ns/sparql-service-description#', '')
                );
                values.push(item.obj.value);
            }
        }

    });

    return {
        labels,
        datasets: [
            {
                label: 'Dataset statistics',
                backgroundColor: "#834ace",
                data: values,
            }
        ]
    };
}

export function returnNumberFormat(number) {
    if ((number/1000000000) >= 1) {
        return number/1000000000+' B';
    } else if ((number/1000000) >= 1) {
        return number/1000000+' M';
    } else if ((number/1000) >= 1) {
        return number/1000+' k';
    } else {
        return number;
    }
}

export function datasetExists(RenderComponent) {
    return class WrapperComponent extends React.Component {
        render() {
            const dataset = getDataset(this.props.match.params.name);
            if (dataset) {
                return <RenderComponent {...this.props}/>
            }
            return <Redirect to="/"/>
        }
    }
}
