import React, {Component} from 'react';
import './compare.scss';
import {
    getDatasetMetadata,
    getDatasetNamespaces,
    returnNumberFormat
} from "../api/utils";
import {Bar, HorizontalBar} from "react-chartjs-2";

class Compare extends Component {

    state = {
        firstDataset: '',
        secondDataset: '',
        comparison: false,
        comparisonVocabulariesChart: {},
        comparisonMetadataChart: {},
        missingVocabularies: {
            dataset1: [],
            dataset2: []
        },
        namedGraphs: {
            dataset1: [],
            dataset2: []
        }
    };

    generateDatasetSelect = () => {
        let options = [];
        let dataset = localStorage.getItem(`datasets`);
        if (dataset) {
            dataset = dataset.substr(0, dataset.length - 1);
            let datasets = dataset.split(',');
            options = datasets.map((d, index) => {
                return <option value={d}
                               key={index}>{d}</option>
            });
        }
        options.unshift(<option value="" key="none"/>);

        return options;
    };

    handleSelectFirstDataset = (e) => {
        this.setState({
            firstDataset: e.target.value
        });
    };

    handleSelectSecondDataset = (e) => {
        this.setState({
            secondDataset: e.target.value
        });
    };

    handleCompareDatasets = () => {
        let dataset1 = getDatasetNamespaces(this.state.firstDataset);
        let dataset2 = getDatasetNamespaces(this.state.secondDataset);

        let labels = [];
        let colors = [];

        let values1 = [];
        let values2 = [];

        if (dataset1.length > dataset2.length) {
            dataset1.forEach(namespace => {
                const dataset2Item = dataset2[dataset2.findIndex(item => item.prefix === namespace.prefix)];
                if (dataset2Item) {
                    values2.push(dataset2Item.triples);

                    labels.push(namespace.prefix);
                    colors.push(namespace.color);
                    values1.push(namespace.triples);

                }
            });
        } else {
            dataset2.forEach(namespace => {
                const dataset1Item = dataset1[dataset1.findIndex(item => item.prefix === namespace.prefix)];
                if (dataset1Item) {
                    values1.push(dataset1Item.triples);

                    labels.push(namespace.prefix);
                    colors.push(namespace.color);
                    values2.push(namespace.triples);
                }
            });
        }

        let missing1 = [];
        let missing2 = [];

        dataset1.forEach(item => {
            if (!labels.includes(item.prefix)) {
                missing2.push(item.prefix);
            }
        });

        dataset2.forEach(item => {
            if (!labels.includes(item.prefix)) {
                missing1.push(item.prefix);
            }
        });

        this.setState({
            comparison: true,
            comparisonVocabulariesChart: {
                labels,
                datasets: [
                    {
                        label: `Dataset: ${this.state.firstDataset}`,
                        backgroundColor: colors,
                        data: values1,
                        borderColor: "#000000",
                        borderWidth: "5"
                    },
                    {
                        label: `Dataset: ${this.state.secondDataset}`,
                        backgroundColor: colors,
                        data: values2,
                        borderColor: "#ff1a02",
                        borderWidth: "5"
                    }
                ]
            },
            missingVocabularies: {
                dataset1: missing1,
                dataset2: missing2
            }
        });

        let dataset1Metadata = getDatasetMetadata(this.state.firstDataset);
        let dataset2Metadata = getDatasetMetadata(this.state.secondDataset);


        let labelsTmp1 = [];
        let valuesTmp1 = [];

        dataset1Metadata.sort((a, b) => b.obj.value - a.obj.value);
        dataset1Metadata.forEach(item => {
            if (item.graph.value === 'default' && parseInt(item.obj.value)) {
                labelsTmp1.push(item.prop.value.replace('http://rdfs.org/ns/void#', '').replace('http://ldf.fi/void-ext#', '').replace('http://www.w3.org/ns/sparql-service-description#', ''));
                valuesTmp1.push(item.obj.value);
            }

        });

        let labelsTmp2 = [];
        let valuesTmp2 = [];

        dataset2Metadata.sort((a, b) => b.obj.value - a.obj.value);
        dataset2Metadata.forEach(item => {
            if (item.graph.value === 'default' && parseInt(item.obj.value)) {
                labelsTmp2.push(item.prop.value.replace('http://rdfs.org/ns/void#', '').replace('http://ldf.fi/void-ext#', '').replace('http://www.w3.org/ns/sparql-service-description#', ''));
                valuesTmp2.push(item.obj.value);
            }

        });

        let metadataLabels = [];
        if (labelsTmp1 >= labelsTmp2) {
            metadataLabels = labelsTmp1;
        } else {
            metadataLabels = labelsTmp2;
        }

        this.setState({
            comparisonMetadataChart: {
                labels: metadataLabels,
                datasets: [
                    {
                        label: `Dataset: ${this.state.firstDataset}`,
                        backgroundColor: "#000000",
                        data: valuesTmp1,
                    },
                    {
                        label: `Dataset: ${this.state.secondDataset}`,
                        backgroundColor: "#ff1a02",
                        data: valuesTmp2,
                    }
                ]
            }
        });


        const graphs1 = dataset1Metadata.reduce((result, item) => {
            if (item.graph.value !== 'default') {
                result.add(item.graph.value)
            }
            return result;
        }, new Set());

        const graphs2 = dataset2Metadata.reduce((result, item) => {
            if (item.graph.value !== 'default') {
                result.add(item.graph.value)
            }
            return result;
        }, new Set());

        this.setState({
            namedGraphs: {
                dataset1: Array.from(graphs1),
                dataset2: Array.from(graphs2),
            }
        })

    };

    render() {
        return (
            <>
                <div className="content-sub-header">
                    <div className="left-side">
                        <h3 className="title">Compare data sets</h3>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card">
                                <div className="card-head">
                                    <div className="card-head-label">
                                        <h3 className="title">Compare</h3>
                                        <small>select two datasets</small>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="col-lg-4">
                                        <label>Data set name:</label>
                                        <select
                                            className="form-control"
                                            value={this.state.firstDataset}
                                            onChange={this.handleSelectFirstDataset}>
                                            {this.generateDatasetSelect()}
                                        </select>
                                    </div>
                                    <div className="col-lg-4">
                                        <label>Data set endpoint:</label>
                                        <select
                                            className="form-control"
                                            value={this.state.secondDataset}
                                            onChange={this.handleSelectSecondDataset}>
                                            {this.generateDatasetSelect()}
                                        </select>
                                    </div>
                                    <div className="col-lg-2 column-bottom">
                                        <button type="submit" className="btn btn-primary btn-up"
                                                style={{width: "100px"}} onClick={this.handleCompareDatasets}>Compare
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.comparison ?
                    <>
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-xl-12">
                                    <div className="card">
                                        <div className="card-head">
                                            <div className="card-head-label">
                                                <h3 className="title">Comparison
                                                    between: {this.state.firstDataset} and {this.state.secondDataset}</h3>
                                            </div>
                                        </div>
                                        <div className="card-body">
                                            <Bar
                                                data={this.state.comparisonVocabulariesChart}
                                                height={300}
                                                options={{
                                                    responsive: true,
                                                    maintainAspectRatio: false,
                                                    scales: {
                                                        yAxes: [
                                                            {
                                                                ticks: {
                                                                    callback: function (label, index, labels) {
                                                                        return returnNumberFormat(label);
                                                                    }
                                                                }
                                                            }
                                                        ]
                                                    },
                                                    tooltips: {
                                                        mode: 'x',
                                                        intersect: false
                                                    }
                                                }}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-xl-12">
                                    <div className="card">
                                        <div className="card-head">
                                            <div className="card-head-label">
                                                <h3 className="title">Metadata comparison</h3>
                                            </div>
                                        </div>
                                        <div className="card-body">
                                            <HorizontalBar
                                                data={this.state.comparisonMetadataChart}
                                                options={{
                                                    maintainAspectRatio: true,
                                                    responsive: true,
                                                    scales: {
                                                        xAxes: [
                                                            {
                                                                ticks: {
                                                                    callback: function (label, index, labels) {
                                                                        return returnNumberFormat(label);
                                                                    }
                                                                }
                                                            }
                                                        ]
                                                    },
                                                    tooltips: {
                                                        mode: 'index',
                                                        axis: 'y',
                                                        intersect: false
                                                    }
                                                }}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-xl-12">
                                    <div className="card">
                                        <div className="card-head">
                                            <div className="card-head-label">
                                                <h3 className="title">Missing vocabularies</h3>
                                            </div>
                                        </div>
                                        <div className="card-body">
                                            <div className="missing-vocabularies">
                                                <h4 className="title">{this.state.firstDataset}</h4>
                                                <div className="dataset-section">
                                                    {this.state.missingVocabularies.dataset1.map(vocab => {
                                                        return <span key={vocab}>{vocab}</span>;
                                                    })}
                                                </div>
                                                <h4 className="title">{this.state.secondDataset}</h4>
                                                <div className="dataset-section">
                                                    {this.state.missingVocabularies.dataset2.map(vocab => {
                                                        return <span key={vocab}>{vocab}</span>;
                                                    })}
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-xl-12">
                                    <div className="card">
                                        <div className="card-head">
                                            <div className="card-head-label">
                                                <h3 className="title">Named graphs</h3>
                                            </div>
                                        </div>
                                        <div className="card-body">
                                            <div className="named-graphs">
                                                <h4 className="title">{this.state.firstDataset}</h4>
                                                <ul className="dataset-section">
                                                    {this.state.namedGraphs.dataset1.map(graph => {
                                                        return <li key={graph}>{graph}</li>;
                                                    })}
                                                </ul>
                                                <h4 className="title">{this.state.secondDataset}</h4>
                                                <ul className="dataset-section">
                                                    {this.state.namedGraphs.dataset2.map(graph => {
                                                        return <li key={graph}>{graph}</li>;
                                                    })}
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>

                    : ""
                }
            </>
        );
    }
}


export default Compare;
