import React, {Component} from 'react';
import {Bar, HorizontalBar} from 'react-chartjs-2';
import { search } from '../api/api';
import './dashboard.scss';
import {
    generateMetadataChart, generateVocabulariesChart,
    getDatasetMetadata, getDatasetNamespaces,
    getPrefixInSpanAndText,
    returnNumberFormat
} from "../api/utils";
import {ErrorAlert, LoadingSpinner, SearchForm} from "../common";

const MAX_TEXT_LENGTH = 32;

class Dashboard extends Component {

    state = {
        searchValue: '',
        searchNamespace: '',
        searchCheckBoxes: [],
        searchRequest: false,
        searchResult: [],
        searchFailed: null,
        loading: false,
    };

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    generateNamespaceSelect = () => {
        let dataset = localStorage.getItem(`dataset:${this.props.match.params.name}`);
        dataset = JSON.parse(dataset).metadata.namespaces;
        dataset.sort((a, b) => b.triples - a.triples);
        let options = dataset.map(namespace => {
            return <option name={namespace.prefix} value={namespace.iri}
                           key={namespace.prefix}>{namespace.prefix}</option>
        });
        options.unshift(<option value="" key="none"/>);

        return options;
    };

    generateDatasetIRIs = (type) => {
        let dataset = localStorage.getItem(`dataset:${this.props.match.params.name}`);
        try {
            dataset = JSON.parse(dataset).metadata[type];
            return this.generateSectionItems(dataset);
        } catch (e) {
            return "";
        }
    };

    generateSectionItems = (data) => {
        return data.map((item, index) => {
            let text = item.object.value;
            let changedText = getPrefixInSpanAndText(text);

            text = changedText.text;
            let prefix = changedText.prefix;

            if (text.length > MAX_TEXT_LENGTH) {
                text = text.substr(0, MAX_TEXT_LENGTH) + '...';
            }
            return (
                <div className="class-item" key={index}>
                    <div className="class-item-name">
                        <span className="hint--bottom" aria-label={item.object.value}>{prefix}{text}</span>
                    </div>
                    <div className="class-item-freq">
                        <span>{item.triples.value}</span>
                    </div>
                </div>
            );
        });
    };

    generateSections = (sections, generateFunc, ...rest) => {
        return sections.map((section, index) => {
            return (
                <div className="col-xl-4" key={index}>
                    <div className="card">
                        <div className="card-head">
                            <div className="card-head-label">
                                <h3 className="title">{section.toLocaleUpperCase()}</h3>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="col-lg-12">
                                {generateFunc(section, ...rest)}
                            </div>
                        </div>
                    </div>
                </div>
            );
        });
    };

    generateSectionItemsFromResult = () => {
        let subjects = [];
        let properties = [];
        let objects = [];

        this.state.searchResult.forEach(result => {
            let prop = result.prop.value;
            /* only 3 possibilities:
                http://rdfs.org/ns/void#property
                http://ldf.fi/void-ext#subject
                http://ldf.fi/void-ext#object
                -> all have # as a separator
             */
            const data = {
                object: result.object,
                triples: result.triples,
            };
            prop = prop.split('#')[1];

            if (prop === 'subject') {
                subjects.push(data);
            } else if (prop === 'object') {
                objects.push(data);
            } else if (prop === 'property') {
                properties.push(data);
            }
        });

        let sections = [];
        if (subjects.length) {
            sections.push('subjects');
        }
        if (properties.length) {
            sections.push('properties');
        }
        if (objects.length) {
            sections.push('objects');
        }

        const sectionData = {
            subjects,
            properties,
            objects
        };

        return this.generateSections(sections, this.generateSearchResultIRIs, sectionData);
    };

    generateSearchResultIRIs = (section, data) => {
        try {
            return this.generateSectionItems(data[section]);
        } catch (e) {
            return "";
        }
    };

    handleChangeNamespace = (e) => {
        this.setState({
            searchNamespace: e.target.value
        });
    };

    handleChangeSearchValue = (e) => {
        this.setState({
            searchValue: e.target.value
        });
    };

    handleSubmitSearch = async (e) => {
        e.preventDefault();
        this.setState({
            loading: true
        });
        const dataset = localStorage.getItem(`dataset:${this.props.match.params.name}`);

        let data = await search(JSON.parse(dataset).endpoint, this.state.searchCheckBoxes, this.state.searchNamespace + this.state.searchValue, 20);

        if (!data.error) {
            this.setState({
                searchRequest: true,
                searchResult: data,
                loading: false,
            });
        } else {
            this.setState({
                searchFailed: data.e,
                loading: false,
            });
        }

    };

    handleClickCheckBoxSearch = (e, name) => {
        if (this.state.searchCheckBoxes.includes(name)) {
            this.setState({
                searchCheckBoxes: [...this.state.searchCheckBoxes.filter(item => item !== name)]
            });

        } else {
            this.setState({
                searchCheckBoxes: [...this.state.searchCheckBoxes, name]
            });
        }

    };

    handleCloseAlert = () => {
        this.setState({
            searchFailed: null,
        });
    };

    render() {
        return (
            <>
                {this.state.searchFailed ?
                    <ErrorAlert
                        message={this.state.searchFailed.message}
                        handleCloseAlert={this.handleCloseAlert}
                    />
                    : ""
                }
                {this.state.loading ? <LoadingSpinner/> : ""}
                <div className="content-sub-header">
                    <div className="left-side">
                        <h3 className="title">Dashboard</h3>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card">
                                <div className="card-head">
                                    <div className="card-head-label">
                                        <h3 className="title">Dataset - {this.props.match.params.name}</h3>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="col-lg-4">
                                        <HorizontalBar
                                            data={generateMetadataChart(getDatasetMetadata(this.props.match.params.name), true)}
                                            height={300}
                                            options={{
                                                maintainAspectRatio: false,
                                                scales: {
                                                    xAxes: [
                                                        {
                                                            ticks: {
                                                                callback: function(label, index, labels) {
                                                                    return returnNumberFormat(label);
                                                                }
                                                            }
                                                        }
                                                    ]
                                                },
                                                tooltips: {
                                                    mode: 'index',
                                                    axis: 'y',
                                                    intersect: false
                                                }
                                            }}
                                        />
                                    </div>
                                    <div className="col-lg-8">
                                        <Bar
                                            data={generateVocabulariesChart(getDatasetNamespaces(this.props.match.params.name))}
                                            height={300}
                                            options={{
                                                responsive: true,
                                                maintainAspectRatio: false,
                                                scales: {
                                                    yAxes: [
                                                        {
                                                            ticks: {
                                                                callback: function(label, index, labels) {
                                                                    return returnNumberFormat(label);
                                                                }
                                                            }
                                                        }
                                                    ]
                                                },
                                                tooltips: {
                                                    mode: 'x',
                                                    intersect: false
                                                }
                                            }}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card">
                                <div className="card-head">
                                    <div className="card-head-label">
                                        <h3 className="title">Search in statistics</h3>
                                    </div>
                                </div>
                                <div className="card-body card-content-center">
                                    <div className="row">
                                        <SearchForm
                                            match={this.props.match.params.name}
                                            handleChangeNamespace={this.handleChangeNamespace}
                                            handleChangeSearchValue={this.handleChangeSearchValue}
                                            handleSubmitSearch={this.handleSubmitSearch}
                                        />

                                        <div className="col-lg-12" style={{marginTop: '4rem'}}>
                                            <div className="checkbox-inline">
                                                <label className="checkbox">
                                                    <input
                                                        type="checkbox"
                                                        onClick={(e) => this.handleClickCheckBoxSearch(e, 'subject')}/> Subjects
                                                    <span/>
                                                </label>
                                                <label className="checkbox">
                                                    <input
                                                        type="checkbox"
                                                        onClick={(e) => this.handleClickCheckBoxSearch(e, 'property')}/> Properties
                                                    <span/>
                                                </label>
                                                <label className="checkbox">
                                                    <input
                                                        type="checkbox"
                                                        onClick={(e) => this.handleClickCheckBoxSearch(e, 'object')}/> Objects
                                                    <span/>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row" style={{justifyContent: 'center'}}>
                        {!this.state.searchRequest ?
                            this.generateSections(['subjects', 'properties', 'objects'], this.generateDatasetIRIs)
                            : this.generateSectionItemsFromResult()}
                    </div>
                </div>

            </>
        );
    }
}

export default Dashboard;
