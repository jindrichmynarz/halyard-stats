import React, { Component } from 'react';
import './dataset.scss';
import {load, matchNamespaceIRI} from '../api/api';
import { Link } from "react-router-dom";

import {ErrorAlert, LoadingSpinner} from '../common';
class Dataset extends Component {

    state = {
        datasetName: '',
        datasetEndpoint: '',
        loading: false,
        datasetNameInvalid: false,
        datasetEndpointInvalid: false,
        datasetLoadFailed: null,
        datasets: '',
    };

    componentDidMount = async () => {
        localStorage.removeItem('customProperties');
        this.setState({
            datasets: localStorage.getItem('datasets')
        });
    };


    handleChangeDatasetName = (e) => {
        this.setState({
            datasetName: e.target.value,
            datasetNameInvalid: false,
        });
    };

    handleChangeDatasetEndpoint = (e) => {
        this.setState({
            datasetEndpoint: e.target.value,
            datasetEndpointInvalid: false,
        });
    };

    handleAddDataset = async (e) => {
        e.preventDefault();

        if (this.state.datasetName.length === 0) {
            this.setState({
                datasetNameInvalid: true
            });
            return false;
        }

        if (this.state.datasetEndpoint.length === 0) {
            this.setState({
                datasetEndpointInvalid: true
            });
            return false;
        }

        this.setState({
            loading: true
        });
        let data = await load(this.state.datasetEndpoint);

        if (!data.error) {

            data = data[0];
            let metadata = data.dataset;
            metadata.namespaces = await matchNamespaceIRI(metadata.namespaces);

            const store = {
                endpoint: this.state.datasetEndpoint,
                metadata
            };

            const datasets = localStorage.getItem('datasets') || '';
            localStorage.setItem(`datasets`, `${datasets}${this.state.datasetName},`);
            localStorage.setItem(`dataset:${this.state.datasetName}`, JSON.stringify(store));
            this.setState({
                datasets: localStorage.getItem('datasets')
            });

        } else {
            this.setState({
                datasetLoadFailed: data.e
            });
        }

        this.setState({
            loading: false,
            datasetName: '',
            datasetEndpoint: '',
        });
    };

    handleDeleteLoadedDataset = (e, name) => {
        let datasets = localStorage.getItem('datasets');
        datasets = datasets.replace(`${name},`, '');
        localStorage.setItem(`datasets`, datasets);
        localStorage.removeItem(`dataset:${name}`);
        this.setState({
            datasets: localStorage.getItem('datasets')
        });
    };

    handleCloseAlert = () => {
        this.setState({
            datasetLoadFailed: null,
        });
    };



    renderUploadedDatasets = () => {
        let datasets = this.state.datasets;

        if (datasets) {
            datasets = datasets.substr(0, datasets.length - 1);
            const datasetParts = datasets.split(',');
            return datasetParts.map(dataset => {
                const datasetData = localStorage.getItem(`dataset:${dataset}`);
                const data = JSON.parse(datasetData).metadata;
                let graphName = '';
                let graphs = false;
                if (Object.keys(data.metadata).length !== 0) {
                    return (
                        <div className="col-xl-6" key={dataset}>
                            <div className="card dataset graph-stats">
                                <div className="card-head">
                                    <div className="card-head-label">
                                        <h3 className="title">{dataset}</h3>
                                    </div>
                                    <div className="card-head-label close">
                                        <i className="fa fa-times"
                                           onClick={(e) => this.handleDeleteLoadedDataset(e, dataset)}/>
                                    </div>
                                </div>
                                <div className="card-body" style={{flexDirection: "column"}}>
                                    <ul>
                                        {data.metadata.map((item, index) => {
                                            if (item.graph.value === 'default') {
                                                return <li key={index}>{item.prop.value.replace('http://rdfs.org/ns/void#', '').replace('http://ldf.fi/void-ext#', '').replace('http://www.w3.org/ns/sparql-service-description#', '')} - {item.obj.value}</li>;
                                            } else {
                                                graphs = true;
                                            }
                                            return '';
                                        })}
                                    </ul>
                                    {graphs ?
                                        <>
                                            <h3 className="title">Graphs</h3>
                                            <ul>
                                                {data.metadata.map((item, index) => {
                                                    if (item.graph.value !== 'default') {
                                                        if (graphName !== item.graph.value) {
                                                            graphName = item.graph.value;
                                                            return (
                                                                <React.Fragment key={index}>
                                                                <li className="graph-name">{item.graph.value}</li>
                                                                <li>{item.prop.value.replace('http://rdfs.org/ns/void#', '').replace('http://ldf.fi/void-ext#', '')} - {item.obj.value}</li>
                                                                </React.Fragment>
                                                            )
                                                        }

                                                        return <li key={index}>{item.prop.value.replace('http://rdfs.org/ns/void#', '').replace('http://ldf.fi/void-ext#', '')} - {item.obj.value}</li>;
                                                    }
                                                    return '';
                                                })}
                                            </ul>
                                        </>
                                        : ""
                                    }
                                </div>
                                <div className="card-footer">
                                    <Link to={`dashboard/${dataset}`} className="btn btn-primary btn-up">
                                        Explore
                                    </Link>
                                </div>
                            </div>
                        </div>
                    );
                }
                return false;
            });
        } else {
            return "";
        }
    };

    render() {
        return (
            <>
                {this.state.datasetLoadFailed ?
                    <ErrorAlert
                        message={this.state.datasetLoadFailed.message}
                        handleCloseAlert={this.handleCloseAlert}
                    />
                    : ""}
                {this.state.loading ? <LoadingSpinner /> : ""}
                <div className="content-sub-header">
                    <div className="left-side">
                        <h3 className="title">Data sets</h3>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card">
                                <div className="card-head">
                                    <div className="card-head-label">
                                        <h3 className="title">Add new data set</h3>
                                        <small>enter name and endpoint url</small>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="col-lg-4">
                                        <label>Data set name:</label>
                                        <input
                                            type="text"
                                            className={this.state.datasetNameInvalid ? 'form-control is-invalid' : 'form-control'}
                                            placeholder="Enter name"
                                            value={this.state.datasetName}
                                            onChange={this.handleChangeDatasetName}/>
                                    </div>
                                    <div className="col-lg-4">
                                        <label>Data set endpoint:</label>
                                        <input
                                            type="text"
                                            className={this.state.datasetEndpointInvalid ? 'form-control is-invalid' : 'form-control'}
                                            placeholder="Enter endpoint"
                                            value={this.state.datasetEndpoint}
                                            onChange={this.handleChangeDatasetEndpoint}/>
                                    </div>
                                    <div className="col-lg-2 column-bottom">
                                        <button type="submit" className="btn btn-primary btn-up" onClick={this.handleAddDataset}>Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.datasets ?
                    <>
                        <div className="content-sub-header">
                            <div className="left-side">
                                <h3 className="title">Uploaded data sets</h3>
                            </div>
                        </div>
                        <div className="container-fluid">
                            <div className="row">
                                {this.renderUploadedDatasets()}
                            </div>
                        </div>
                    </>
                    : ""
                }
            </>
        );
    }
}

export default Dataset;
