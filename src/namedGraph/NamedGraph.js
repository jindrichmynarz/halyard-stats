import React, { Component } from 'react';
import './namedGraph.scss';
import {ErrorAlert, LoadingSpinner} from "../common";
import {Bar, HorizontalBar} from "react-chartjs-2";
import {
    returnNumberFormat,
    generateVocabulariesChart,
    generateMetadataChart,
    getDatasetMetadata, getDataset
} from "../api/utils";
import {getGraphVocabs, matchNamespaceIRI} from "../api/api";

class NamedGraph extends Component {

    state = {
        vocabularies: [],
        selectedVocabularyPrefix: '',
        selectedVocabularyIRI: '',
        vocabularyItems: [],
        vocabularyItemsCount: [],
        endpoint: null,
        datasetLoadFailed: null,
        loading: false,
        limit: 20,
        offset: 0,
        graphs: [],
        selectedGraph: null,
        selectedGraphVocabularies: [],

    };

    componentDidMount = () => {
        const dataset = getDataset(this.props.match.params.name);
        const data = dataset.metadata;
        const graphs = data.metadata.reduce((result, item) => {
            if (item.graph.value !== 'default') {
                result.add(item.graph.value)
            }
            return result;
        }, new Set());

        this.setState({
            graphs: Array.from(graphs),
            endpoint: dataset.endpoint,
        });
    };


    handleCloseAlert = () => {
        this.setState({
            datasetLoadFailed: null,
        });
    };

    handleSelectGraph = async (e) => {
        e.persist();
        this.setState({
            loading: true,
        });
        const that = this;
        this.setState({
            selectedGraph: null,
        }, () => {
            that.setState({
                selectedGraph: e.target.value
            });
        });

        const vocabs = await getGraphVocabs(this.state.endpoint, e.target.value);


        if (!vocabs.error) {
            const vocabIRIs = await matchNamespaceIRI(vocabs);

            if (!vocabIRIs.error) {
                this.setState({
                    selectedGraphVocabularies: vocabIRIs,
                    loading: false,
                });
            } else {
                this.setState({
                    datasetLoadFailed: vocabIRIs.e,
                    loading: false,
                });
            }
        } else {
            this.setState({
                datasetLoadFailed: vocabs.e,
                loading: false,
            });
        }
    };

    renderGraphCharts = () => {
        const metadata = getDatasetMetadata(this.props.match.params.name);

        const graphMetadata = metadata.reduce((result, item) => {
            if (item.graph.value === this.state.selectedGraph) {
                result.push(item)
            }
            return result;
        }, []);

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xl-12">
                        <div className="card">
                            <div className="card-head">
                                <div className="card-head-label">
                                    <h3 className="title">Graph - {this.state.selectedGraph}</h3>
                                </div>
                            </div>
                            <div className="card-body">
                                <div className="col-lg-4">
                                    <HorizontalBar
                                        data={generateMetadataChart(graphMetadata)}
                                        height={300}
                                        options={{
                                            maintainAspectRatio: false,
                                            scales: {
                                                xAxes: [
                                                    {
                                                        ticks: {
                                                            callback: function (label, index, labels) {
                                                                return returnNumberFormat(label);
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            tooltips: {
                                                mode: 'index',
                                                axis: 'y',
                                                intersect: false
                                            }
                                        }}
                                    />
                                </div>
                                <div className="col-lg-8">
                                    <Bar
                                        data={generateVocabulariesChart(this.state.selectedGraphVocabularies)}
                                        height={300}
                                        options={{
                                            responsive: true,
                                            maintainAspectRatio: false,
                                            scales: {
                                                yAxes: [
                                                    {
                                                        ticks: {
                                                            callback: function (label, index, labels) {
                                                                return returnNumberFormat(label);
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            tooltips: {
                                                mode: 'x',
                                                intersect: false
                                            }
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    };

    render() {
        return (
            <>
                {this.state.datasetLoadFailed ?
                    <ErrorAlert
                        message={this.state.datasetLoadFailed.message}
                        handleCloseAlert={this.handleCloseAlert}
                    />
                    : ""
                }
                {this.state.loading ? <LoadingSpinner/> : ""}
                <div className="content-sub-header">
                    <div className="left-side">
                        <h3 className="title">Named graphs</h3>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card">
                                <div className="card-head">
                                    <div className="card-head-label">
                                        <h3 className="title">Dataset vocabularies</h3>
                                        <small>select named graph and explore its properties</small>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="col-xl-12">
                                        {this.state.graphs.map(graph => {
                                            return <button
                                                type="button"
                                                className="btn btn-info"
                                                key={graph}
                                                onClick={this.handleSelectGraph}
                                                value={graph}
                                                style={{
                                                    backgroundColor: '#fff',
                                                    color: '#000',
                                                    marginRight: "0.5rem",
                                                    border: `1px solid #000`,
                                                    borderRadius: '10px',
                                                    fontWeight: "bold"
                                                }}
                                            >
                                                {graph}</button>
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.selectedGraph ?
                    this.renderGraphCharts()
                    : ""
                }
            </>
        );
    }
}

export default NamedGraph;
