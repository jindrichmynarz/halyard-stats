// import { Polly } from '@pollyjs/core';
// import FetchAdapter from '@pollyjs/adapter-fetch';
// Polly.register(FetchAdapter);


context('Dataset', () => {
    beforeEach(() => {
        cy.visit('http://localhost:4000/', {onBeforeLoad: (win) => {
                win.fetch = null;
            }})
    });

    it('Cannot add dataset when not working endpoint and close the alert!', () => {
            // localStorage.setItem('prefixes', JSON.stringify('test'));

        cy.server();
        cy.route('GET', 'http://www.test.test/**').as('sparql');

        cy.get('input:first').type('PubMed');
        cy.get('input').eq(1).type('http://www.test.test');
        cy.get('button').click();

        cy.wait('@sparql').its('status').should('eq', 500);

        cy.get('.alert-text').contains('Invalid SPARQL endpoint').should('be.visible');
        cy.get('.fa-times').click();
        cy.get('.alert-text').should('to.not.exist');
    });

    it('Cannot add dataset without a name!', () => {
        cy.get('button').click();
        cy.get('input:first').should('have.class', 'is-invalid')
    });

    it('Add new dataset', () => {
        cy.server();

        cy.get('input:first').type('PubMed');
        cy.get('input').eq(1).type('http://localhost:3030/pubmed/query');
        cy.get('button').click();

        cy.wait(25000);

        cy.get('.col-xl-6 > .card > .card-head > :nth-child(1) > .title').contains('PubMed').should('be.visible');

    });
});

/*
import { Polly } from '@pollyjs/core';
import FetchAdapter from '@pollyjs/adapter-fetch';
Polly.register(FetchAdapter);

context('Dataset', () => {
    beforeEach(() => {
        // localStorage.removeItem('prefixes');
        cy.visit('http://localhost:4000/', {onBeforeLoad: (win) => {
                win.fetch = new Polly('brambora', {
                    adapters: ['fetch']
                }).adapters["fetch"];
            }})
        cy.visit('http://localhost:4000/');
    });

    it('will load the vocabulary prefixes!', () => {
        cy.server();
        cy.route('GET', 'https://prefix.cc/popular/all.file.json').as('prefixes');
        cy.wait('@prefixes');
    });

    // it('Cannot add dataset without name!', () => {
    //     cy.wait(5000);
    //     // localStorage.setItem('prefixes', JSON.stringify('test'));
    //     cy.get('button').click();
    //     cy.get('input:first').should('have.class', 'is-invalid')
    // });
    //
    // it('Cannot add dataset when not working endpoint and close the alert!', () => {
    //     // localStorage.setItem('prefixes', JSON.stringify('test'));
    //     cy.get('input:first').type('PubMed');
    //     cy.get('input').eq(1).type('http://www.example.com');
    //     cy.get('button').click();
    //     cy.wait(5000);
    //     cy.get('.alert-text').contains('Invalid SPARQL endpoint').should('be.visible');
    //     cy.get('.fa-times').click();
    //     cy.get('.alert-text').should('to.not.exist');
    // });
});

 */
